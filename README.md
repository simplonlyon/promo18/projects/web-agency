# Agence Web

Ce projet consiste à réaliser en HTML/CSS le site web d'une agence web, en groupe, en faisant en portant une attention à la structure des pages et à l'aspect responsif de celles ci.

## Pages attendues
* Accueil : une landing page, vous êtes libres sur son contenu. Ne pas hésiter à consulter les sites d'autres agences web pour voir ce qui est fait.
* Nos Réalisations : page listants les projets déjà réalisés par l'agences et/ou les clients de celles ci
* Notre équipe : page présentant les différents membres de l'agences web (vous pouvez mettre des gens imaginaires, ne pas hésiter à regarder les rôles qu'il pourrait y avoir dans une telle structure)
* À propos / Préstations : page présentant les prestations possibles de l'agence web
* Contact : un formulaire de contact et localisation géographique de l'agence

### Maquettes fonctionnelles

#### Page Our Team - Desktop
<img src="wireframes/our-team-desktop.jpg" alt="Page Our Team - Desktop" width="200">

#### Page Our Team - Mobile
<img src="wireframes/our-team-mobile.jpg" alt="Page Our Team - Mobile" width="200">

## Travail Attendu
* En s'inspirant de l'existant, créer les maquettes fonctionnelles pour au moins 1 page par membre de groupe (maquettes d'exemples faites avec draw.io, mais ce n'est pas obligatoire d'utiliser cette appli)
* Créer la structure HTML des différentes pages en utilisant les balises sémantiques adaptées. **Obligatoire : Valider cette structure sur le [W3C Validator](https://validator.w3.org)**
* Utiliser du css et, de préférence, une librairie css type bootstrap, pour créer la mise en page responsive. Tenter d'apporter un soin à l'harmonisation entre les pages (pas avoir l'impression qu'il y a plusieurs design différents)
* Déployer le site au fur et à mesure sur via une Gitlab Page (exemple de configuration du CI/CD dans ce projet)


## Compétences mobilisées
* Maquetter une application
* Réaliser une interface utilisateur web statique et adaptable
* Collaborer sur un projet informatique en utilisant git et gitlab
 
